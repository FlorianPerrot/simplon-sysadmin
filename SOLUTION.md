## Création du vhost apache2 pour le projet demo

Dans `/etc/apache2/sites-available/demo.conf`
```
## Changer le port si necessaire et changer le `DocumentRoot` si necessaire.
<VirtualHost *:80>
    DocumentRoot /var/www/demo/public
    DirectoryIndex /index.php

    <Directory /var/www/demo/public>
        AllowOverride None
        Order Allow,Deny
        Allow from All

        FallbackResource /index.php
    </Directory>
</VirtualHost>
```

- Activer le site `sudo a2ensite demo`
- recharger apache `sudo systemctl reload apache2`